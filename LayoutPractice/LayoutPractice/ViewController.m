//
//  ViewController.m
//  LayoutPractice
//
//  Created by Jelena Mehic on 10/7/15.
//  Copyright © 2015 Vast. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *inputView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self.loginButton layer] setBorderColor:[UIColor grayColor].CGColor];
    
    self.inputView.layer.borderColor = [UIColor grayColor].CGColor;
    
}


@end
