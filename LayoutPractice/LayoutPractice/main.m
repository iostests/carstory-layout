//
//  main.m
//  LayoutPractice
//
//  Created by Jelena Mehic on 10/7/15.
//  Copyright © 2015 Vast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
