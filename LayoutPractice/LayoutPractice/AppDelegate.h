//
//  AppDelegate.h
//  LayoutPractice
//
//  Created by Jelena Mehic on 10/7/15.
//  Copyright © 2015 Vast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

